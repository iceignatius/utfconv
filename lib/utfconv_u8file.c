#include <errno.h>
#include "utfconv_base.h"
#include "utfconv_u8file.h"

FILE* utfconv_fopen(const char *filename, const char *mode)
{
    /**
     * @brief Open a file.
     * @details Please refer to the documents of C of the same function.
     */
#ifdef _WIN32
    wchar_t wcsname[FILENAME_MAX];
    if( !utfconv_utf8_to_wcs(wcsname, FILENAME_MAX, filename) ) return NULL;

    wchar_t wcsmode[16];
    if( !utfconv_utf8_to_wcs(wcsmode, 16, mode) ) return NULL;

    return _wfopen(wcsname, wcsmode);
#else
    return fopen(filename, mode);
#endif
}

FILE* utfconv_freopen(const char *filename, const char *mode, FILE *stream)
{
    /**
     * @brief Reopen a file.
     * @details Please refer to the documents of C of the same function.
     */
#ifdef _WIN32
    wchar_t wcsname[FILENAME_MAX];
    if( !utfconv_utf8_to_wcs(wcsname, FILENAME_MAX, filename) ) return NULL;

    wchar_t wcsmode[16];
    if( !utfconv_utf8_to_wcs(wcsmode, 16, mode) ) return NULL;

    return _wfreopen(wcsname, wcsmode, stream);
#else
    return freopen(filename, mode, stream);
#endif
}

int utfconv_remove(const char *filename)
{
    /**
     * @brief Remove a file.
     * @details Please refer to the documents of C of the same function.
     */
#ifdef _WIN32
    wchar_t wcsname[FILENAME_MAX];
    if( !utfconv_utf8_to_wcs(wcsname, FILENAME_MAX, filename) )
    {
        errno = ENOENT;
        return -1;
    }

    return _wremove(wcsname);
#else
    return remove(filename);
#endif
}

int utfconv_rename(const char *oldname, const char *newname)
{
    /**
     * @brief Rename a file.
     * @details Please refer to the documents of C of the same function.
     */
#ifdef _WIN32
    wchar_t oldwcsname[FILENAME_MAX];
    if( !utfconv_utf8_to_wcs(oldwcsname, FILENAME_MAX, oldname) )
    {
        errno = ENOENT;
        return -1;
    }

    wchar_t newwcsname[FILENAME_MAX];
    if( !utfconv_utf8_to_wcs(newwcsname, FILENAME_MAX, newname) )
    {
        errno = ENOENT;
        return -1;
    }

    return _wrename(oldwcsname, newwcsname);
#else
    return rename(oldname, newname);
#endif
}

int utfconv_stat(const char *pathname, struct stat *statbuf)
{
    /**
     * @brief Get file information.
     * @details Please refer to the documents of POSIX of the same function.
     */
#ifdef _WIN32
    wchar_t wcsname[FILENAME_MAX];
    if( !utfconv_utf8_to_wcs(wcsname, FILENAME_MAX, pathname) )
    {
        errno = ENOENT;
        return -1;
    }

    return wstat(wcsname, statbuf);
#else
    return stat(pathname, statbuf);
#endif
}
