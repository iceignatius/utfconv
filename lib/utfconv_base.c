#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include "utfconv_base.h"

#if WCHAR_MAX == UINT32_MAX || WCHAR_MAX == INT32_MAX
#   define WCHAR_SIZE 4
#elif WCHAR_MAX == UINT16_MAX || WCHAR_MAX == INT16_MAX
#   define WCHAR_SIZE 2
#elif WCHAR_MAX == UINT8_MAX || WCHAR_MAX == INT8_MAX
#   define WCHAR_SIZE 1
#else
#   error Size of wchar_t abnormal!
#endif

#define UTFCONV_BUF_SIZE 512

size_t utfconv_utf32char_to_utf16char(char16_t dst[2], char32_t src)
{
    /**
     * @brief Translate a single UTF-32 code to UTF-16 data.
     *
     * @param dst 用來接收轉換結果的陣列，陣列大小為 2 以內即可，而這個輸入也可為 NULL。
     * @param src 來源字元。
     * @return 傳回被寫入陣列的元素數量，若 dst 為 NULL 則傳回 dst 陣列所需的大小。
     *
     * @note 若 src 為無效值，則函式會將其值對應至某個有效值域內，但對應方法未定義。
     */
    size_t nwrite;

    // Map the invalid code to the valid range
    src &= 0x7FFFFFFF;
    if( src > 0x10FFFF ) src %= 0x10FFFF + 1;

    // Calculate the output count
    nwrite = ( src > 0xFFFF )?( 2 ):( 1 );

    // Translate data
    if( dst )
    {
        if( nwrite == 1 )
        {
            dst[0] = src;
        }
        else
        {
            src   -= 0x10000;                   // Now 0 <= src <= 0xFFFFF (20 bits)
            dst[0] = 0xD800 | ( src >> 10 );    // The leading surrogate is 0xD800 plus the higher 10 bits of src
            dst[1] = 0xDC00 | ( src & 0x03FF ); // The trailing surrogate is 0xDC00 plus the lower 10 bits of src
        }
    }

    return nwrite;
}

size_t utfconv_utf32char_to_utf8char(char dst[6], char32_t src)
{
    /**
     * @brief Translate a single UTF-32 code to UTF-8 data.
     *
     * @param dst 用來接收轉換結果的陣列，陣列大小為 6 以內即可，而這個輸入也可為 NULL。
     * @param src 來源字元。
     * @return 傳回被寫入陣列的元素數量，若 dst 為 NULL 則傳回 dst 陣列所需的大小。
     *
     * @note 若 src 為無效值，則函式會將其值對應至某個有效值域內，但對應方法未定義。
     */
    size_t nwrite;

    // Map the invalid code to the valid range
    src &= 0x7FFFFFFF;

    // Calculate the output count
    if     ( !( src & 0xFFFFFF80 ) ) nwrite   = 1;
    else if( !( src & 0xFFFFF800 ) ) nwrite   = 2;
    else if( !( src & 0xFFFF0000 ) ) nwrite   = 3;
    else if( !( src & 0xFFE00000 ) ) nwrite   = 4;
    else if( !( src & 0xFC000000 ) ) nwrite   = 5;
    else                             nwrite   = 6;

    // Translate data
    if( dst )
    {
        switch( nwrite )
        {
        case 6:
            dst[0] = 0xFC | (  src>>30 );
            dst[1] = 0x80 | ( (src>>24) & 0x3F );
            dst[2] = 0x80 | ( (src>>18) & 0x3F );
            dst[3] = 0x80 | ( (src>>12) & 0x3F );
            dst[4] = 0x80 | ( (src>> 6) & 0x3F );
            dst[5] = 0x80 | (  src      & 0x3F );
            break;

        case 5:
            dst[0] = 0xF8 | (  src>>24 );
            dst[1] = 0x80 | ( (src>>18) & 0x3F );
            dst[2] = 0x80 | ( (src>>12) & 0x3F );
            dst[3] = 0x80 | ( (src>> 6) & 0x3F );
            dst[4] = 0x80 | (  src      & 0x3F );
            break;

        case 4:
            dst[0] = 0xF0 | (  src>>18 );
            dst[1] = 0x80 | ( (src>>12) & 0x3F );
            dst[2] = 0x80 | ( (src>> 6) & 0x3F );
            dst[3] = 0x80 | (  src      & 0x3F );
            break;

        case 3:
            dst[0] = 0xE0 | (  src>>12 );
            dst[1] = 0x80 | ( (src>> 6) & 0x3F );
            dst[2] = 0x80 | (  src      & 0x3F );
            break;

        case 2:
            dst[0] = 0xC0 | ( src >> 6 );
            dst[1] = 0x80 | ( src & 0x3F );
            break;

        default:
            dst[0] = src;

        }
    }

    return nwrite;
}

static
bool is_leading_surrogate(char16_t ch)
{
    return ( ch & 0xFC00 ) == 0xD800;
}

static
bool is_trailing_surrogate(char16_t ch)
{
    return ( ch & 0xFC00 ) == 0xDC00;
}

size_t utfconv_utf16char_to_utf32char(char32_t *dst, const char16_t *src)
{
    /**
     * @brief Translate UTF-16 data to a single UTF-32 code.
     *
     * @param dst 用來接收轉換結果的變數，可為 NULL。
     * @param src 來源資料。
     * @return 傳回本次轉換中從來源資料陣列所取用的資料數量。
     *
     * @note 在遇到無法正確轉換的資料時，該單一資料位元組將被直接視為一個號碼作為轉換。
     */
    const uint16_t *usrc = (const uint16_t*) src;
    size_t          nread;

    // Calculate the input count
    if( !usrc )
    {
        nread = 0;
    }
    if( is_leading_surrogate(usrc[0]) && is_trailing_surrogate(usrc[1]) )
    {
        nread = 2;
    }
    else
    {
        nread = 1;
    }

    // Translate data
    if( nread && dst )
    {
        if( nread == 1 )
        {
            *dst = usrc[0];
        }
        else
        {
            *dst = 0x10000 + ( (( (usrc[0]) & 0x03FF )<<10) | ( (usrc[1]) & 0x03FF ) );
        }
    }

    return nread;
}

static
int count_leadingbits(uint8_t byte)
{
    /*
     * 計算一個位元組在二進位資料中從頭起算的連續 1 的數量
     */
    if     ( byte  < 0x80 ) return 0;  // Bin : 0XXXXXXX
    else if( byte  < 0xC0 ) return 1;  // Bin : 10XXXXXX
    else if( byte  < 0xE0 ) return 2;  // Bin : 110XXXXX
    else if( byte  < 0xF0 ) return 3;  // Bin : 1110XXXX
    else if( byte  < 0xF8 ) return 4;  // Bin : 11110XXX
    else if( byte  < 0xFC ) return 5;  // Bin : 111110XX
    else if( byte  < 0xFE ) return 6;  // Bin : 1111110X
    else if( byte == 0xFE ) return 7;  // Bin : 11111110
    else                    return 8;  // Bin : 11111111
}

static
int count_u8pairs(const uint8_t *data)
{
    /*
     * 計算資料中每個位元組皆為 10XXXXXX (二進位) 格式的連續排列數量
     */
    int cnt;

    for(( cnt=0 );( ( *data++ & 0xC0 ) == 0x80 );( ++cnt ));

    return cnt;
}

size_t utfconv_utf8char_to_utf32char(char32_t *dst, const char *src)
{
    /**
     * @brief Convert UTF-8 data to a single UTF-32 code.
     *
     * @param dst 用來接收轉換結果的變數，可為 NULL。
     * @param src 來源資料。
     * @return 傳回本次轉換中從來源資料陣列所取用的資料數量。
     *
     * @note 在遇到無法正確轉換的資料時，該單一資料位元組將被直接視為一個號碼作為轉換。
     */
    const uint8_t *usrc = (const uint8_t*) src;
    size_t         nread;

    // Calculate the input count
    if( !usrc )
    {
        nread = 0;
    }
    else
    {
        nread = count_leadingbits(usrc[0]);

        if( nread < 2 || 6 < nread )
        {
            nread = 1;
        }
        else
        {
            nread = ( nread > (size_t)count_u8pairs(&usrc[1]) + 1 )?
                    ( 1 ):( nread );
        }
    }

    // Translate data
    if( nread && dst )
    {
        switch( nread )
        {
        case 6:
            *dst = (( usrc[0] & 0x01 )<<30) | (( usrc[1] & 0x3F )<<24) | (( usrc[2] & 0x3F )<<18) | (( usrc[3] & 0x3F )<<12) | (( usrc[4] & 0x3F )<<6) | ( usrc[5] & 0x3F );
            break;
        case 5:
            *dst = (( usrc[0] & 0x03 )<<24) | (( usrc[1] & 0x3F )<<18) | (( usrc[2] & 0x3F )<<12) | (( usrc[3] & 0x3F )<<6) | ( usrc[4] & 0x3F );
            break;
        case 4:
            *dst = (( usrc[0] & 0x07 )<<18) | (( usrc[1] & 0x3F )<<12) | (( usrc[2] & 0x3F )<<6) | ( usrc[3] & 0x3F );
            break;
        case 3:
            *dst = (( usrc[0] & 0x0F )<<12) | (( usrc[1] & 0x3F )<<6) | ( usrc[2] & 0x3F );
            break;
        case 2:
            *dst = (( usrc[0] & 0x1F )<<6) | ( usrc[1] & 0x3F );
            break;
        default:
            *dst = usrc[0];
        }
    }

    return nread;
}

size_t utfconv_utf8_to_utf8(char *dst, size_t dstsize, const char *src)
{
    /**
     * @brief Byte-to-byte data copy.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 byte 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小
     *          (含 null-terminator)，以 byte 為單位。
     *
     * @note
     * 這個函是的目的為建立一個類似 memcpy 的工具。
     * 因為 Wide-Character、Windows TCHAR 等類型在某些狀態下會是一個位元組大小，
     * 因此提供一個界面與本模組函式相似，
     * 但內容僅單純複製資料的函式，供特殊條件下使用。
     */
    size_t size = strlen(src) + 1;
    if( !dst ) return size;
    if( dstsize < size ) return 0;

    memcpy(dst, src, size);
    return size;
}

size_t utfconv_utf32_to_utf16(char16_t *dst, size_t dstsize, const char32_t *src)
{
    /**
     * @brief Translate UTF-32 string to UTF-16 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 char16_t 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 char16_t 為單位。
     */
    size_t nwrite_total = 0;

    if( !src ) return 0;

    if( !dst )
    {
        // 只計算所需的緩衝大小
        while( *src )
        {
            nwrite_total += utfconv_utf32char_to_utf16char(NULL, *src++);
        }
        ++nwrite_total;  // Don't forget the null-terminator
    }
    else
    {
        // 逐一轉換來源字元，並將結果寫入緩衝空間。
        while( *src )
        {
            size_t    nwrite;
            char16_t  ch16[2];
            char16_t *buf;

            // Translate UTF-32 character to UTF-16 format
            nwrite = utfconv_utf32char_to_utf16char(ch16, *src++);
            if( dstsize <= nwrite ) return 0;

            // Size calculation
            dstsize      -= nwrite;
            nwrite_total += nwrite;

            // Fill data
            for(( buf = ch16 );( nwrite-- );( *dst++ = *buf++ ));
        }
        // Write the null-terminator
        *dst = 0;
        ++nwrite_total;
    }

    return nwrite_total;
}

size_t utfconv_utf32_to_utf8(char *dst, size_t dstsize, const char32_t *src)
{
    /**
     * @brief Translate UTF-32 string to UTF-8 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 byte 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 byte 為單位。
     */
    size_t nwrite_total = 0;

    if( !src ) return 0;

    if( !dst )
    {
        // 只計算所需的緩衝大小
        while( *src )
        {
            nwrite_total += utfconv_utf32char_to_utf8char(NULL, *src++);
        }
        ++nwrite_total;  // Don't forget the null-terminator
    }
    else
    {
        // 逐一轉換來源字元，並將結果寫入緩衝空間。
        while( *src )
        {
            size_t nwrite;
            char   ch8[6];
            char  *buf;

            // Translate UTF-32 character to UTF-8 format
            nwrite = utfconv_utf32char_to_utf8char(ch8, *src++);
            if( dstsize <= nwrite ) return 0;

            // Size calculation
            dstsize      -= nwrite;
            nwrite_total += nwrite;

            // Fill data
            for(( buf = ch8 );( nwrite-- );( *dst++ = *buf++ ));
        }
        // Write the null-terminator
        *dst = 0;
        ++nwrite_total;
    }

    return nwrite_total;
}

size_t utfconv_utf16_to_utf32(char32_t *dst, size_t dstsize, const char16_t *src)
{
    /**
     * @brief Translate UTF-16 string to UTF-32 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 char32_t 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 char32_t 為單位。
     */
    size_t nwrite_total = 0;

    if( !src ) return 0;

    if( !dst )
    {
        // 只計算所需的緩衝大小
        while( *src )
        {
            src += utfconv_utf16char_to_utf32char(NULL, src);
            ++nwrite_total;
        }
        ++nwrite_total;  // Don't forget the null-terminator
    }
    else
    {
        size_t destsz0 = dstsize;

        // 逐一轉換來源字元，並將結果寫入緩衝空間。
        while( *src )
        {
            // Translate UTF-16 character to UTF-32 format
            if( !dstsize ) return 0;
            src += utfconv_utf16char_to_utf32char(dst++, src);

            // Size calculation
            --dstsize;
        }
        // Write the null-terminator
        if( !dstsize ) return 0;
        *dst = 0;
        --dstsize;
        // Calculate the total write count
        nwrite_total = destsz0 - dstsize;
    }

    return nwrite_total;
}

size_t utfconv_utf8_to_utf32(char32_t *dst, size_t dstsize, const char *src)
{
    /**
     * @brief Translate UTF-8 string to UTF-32 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 char32_t 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 char32_t 為單位。
     */
    size_t nwrite_total = 0;

    if( !src ) return 0;

    if( !dst )
    {
        // 只計算所需的緩衝大小
        while( *src )
        {
            src += utfconv_utf8char_to_utf32char(NULL, src);
            ++nwrite_total;
        }
        ++nwrite_total;  // Don't forget the null-terminator
    }
    else
    {
        size_t destsz0 = dstsize;

        // 逐一轉換來源字元，並將結果寫入緩衝空間。
        while( *src )
        {
            // Translate UTF-8 character to UTF-32 format
            if( !dstsize ) return 0;
            src += utfconv_utf8char_to_utf32char(dst++, src);

            // Size calculation
            --dstsize;
        }
        // Write the null-terminator
        if( !dstsize ) return 0;
        *dst = 0;
        --dstsize;
        // Calculate the total write count
        nwrite_total = destsz0 - dstsize;
    }

    return nwrite_total;
}

size_t utfconv_utf16_to_utf8(char *dst, size_t dstsize, const char16_t *src)
{
    /**
     * @brief Translate UTF-16 string to UTF-8 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 byte 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 byte 為單位。
     */
    size_t nwrite_total = 0;

    if( !src ) return 0;

    if( !dst )
    {
        // 只計算所需的緩衝大小
        while( *src )
        {
            char32_t ch32;

            src          += utfconv_utf16char_to_utf32char(&ch32, src);
            nwrite_total += utfconv_utf32char_to_utf8char(NULL, ch32);
        }
        ++nwrite_total;  // Don't forget the null-terminator
    }
    else
    {
        // 逐一轉換來源字元，並將結果寫入緩衝空間。
        while( *src )
        {
            size_t   nwrite;
            char32_t ch32;
            char     ch8[6];
            char    *buf;

            // Translate UTF-16 character to UTF-32 format
            src   += utfconv_utf16char_to_utf32char(&ch32, src);

            // Translate UTF-32 character to UTF-8 format
            nwrite = utfconv_utf32char_to_utf8char(ch8, ch32);
            if( dstsize <= nwrite ) return 0;

            // Size calculation
            dstsize      -= nwrite;
            nwrite_total += nwrite;

            // Fill data
            for(( buf = ch8 );( nwrite-- );( *dst++ = *buf++ ));
        }
        // Write the null-terminator
        *dst = 0;
        ++nwrite_total;
    }

    return nwrite_total;
}

size_t utfconv_utf8_to_utf16(char16_t *dst, size_t dstsize, const char *src)
{
    /**
     * @brief Translate UTF-8 string to UTF-16 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 char16_t 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 char16_t 為單位。
     */
    size_t nwrite_total = 0;

    if( !src ) return 0;

    if( !dst )
    {
        // 只計算所需的緩衝大小
        while( *src )
        {
            char32_t ch32;

            src          += utfconv_utf8char_to_utf32char(&ch32, src);
            nwrite_total += utfconv_utf32char_to_utf16char(NULL, ch32);
        }
        ++nwrite_total;  // Don't forget the null-terminator
    }
    else
    {
        // 逐一轉換來源字元，並將結果寫入緩衝空間。
        while( *src )
        {
            size_t    nwrite;
            char32_t  ch32;
            char16_t  ch16[2];
            char16_t *buf;

            // Translate UTF-8 character to UTF-32 format
            src   += utfconv_utf8char_to_utf32char(&ch32, src);

            // Translate UTF-32 character to UTF-16 format
            nwrite = utfconv_utf32char_to_utf16char(ch16, ch32);
            if( dstsize <= nwrite ) return 0;

            // Size calculation
            dstsize      -= nwrite;
            nwrite_total += nwrite;

            // Fill data
            for(( buf = ch16 );( nwrite-- );( *dst++ = *buf++ ));
        }
        // Write the null-terminator
        *dst = 0;
        ++nwrite_total;
    }

    return nwrite_total;
}

size_t utfconv_utf8_to_wcs(wchar_t *dst, size_t dstsize, const char *src)
{
    /**
     * @brief Translate UTF-8 string to wide-character format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 wchar_t 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 wchar_t 為單位。
     */
#if   WCHAR_SIZE == 4
    return utfconv_utf8_to_utf32((char32_t*)dst, dstsize, src);
#elif WCHAR_SIZE == 2
    return utfconv_utf8_to_utf16((char16_t*)dst, dstsize, src);
#elif WCHAR_SIZE == 1
    return utfconv_utf8_to_utf8((char*)dst, dstsize, src);
#else
    #error No implementation on this platform!
#endif
}

size_t utfconv_wcs_to_utf8(char *dst, size_t dstsize, const wchar_t *src)
{
    /**
     * @brief Translate wide-character string to UTF-8 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 byte 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 byte 為單位。
     */
#if   WCHAR_SIZE == 4
    return utfconv_utf32_to_utf8(dst, dstsize, (const char32_t*)src);
#elif WCHAR_SIZE == 2
    return utfconv_utf16_to_utf8(dst, dstsize, (const char16_t*)src);
#elif WCHAR_SIZE == 1
    return utfconv_utf8_to_utf8(dst, dstsize, (const char*)src);
#else
    #error No implementation on this platform!
#endif
}

size_t utfconv_utf8_to_mbs(char *dst, size_t dstsize, const char *src)
{
    /**
     * @brief Translate UTF-8 string to traditional multi-bytes format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 byte 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 byte 為單位。
     *
     * @note 本函式在第一次被呼叫時會將本行程的 Locale 設定變更為作業系統預設值。
     */
    static bool runned      = false;
    wchar_t     buf_static[UTFCONV_BUF_SIZE];
    wchar_t    *buf_dynamic = NULL;
    wchar_t    *buf         = buf_static;
    size_t      wcssz, mbssz;

    // Set locale to system default when first run
    if( !runned )
    {
        runned = true;
#ifdef WINCE
        // Windows CE did not support "setlocale"
#else
        setlocale(LC_ALL, "");
#endif
    }

    // Calculate buffer size needed
    wcssz = utfconv_utf8_to_wcs(NULL, 0, src);
    if( !wcssz ) return 0;
    // Buffer allocate if needed
    if( wcssz > UTFCONV_BUF_SIZE )
    {
        buf = buf_dynamic = malloc( wcssz * sizeof(wchar_t) );
        if( !buf ) return 0;
    }

    // Translate to wide-character format
    utfconv_utf8_to_wcs(buf, wcssz, src);
    // Calculate destination size needed
    mbssz = wcstombs(NULL, buf, 0) + 1;

    // Translate to multi-bytes format
    if( !dst )
    {
        // Nothing to do
    }
    else if( dstsize < mbssz )
    {
        mbssz = 0;
    }
    else
    {
        wcstombs(dst, buf, mbssz);
    }

    // Free the buffer if we allocated it
    if( buf_dynamic )
    {
        free(buf_dynamic);
    }

    return mbssz;
}

size_t utfconv_mbs_to_utf8(char *dst, size_t dstsize, const char *src)
{
    /**
     * @brief Translate traditional multi-bytes string to UTF-8 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 byte 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 byte 為單位。
     *
     * @note 本函式在第一次被呼叫時會將本行程的 Locale 設定變更為作業系統預設值。
     */
    static bool runned      = false;
    wchar_t     buf_static[UTFCONV_BUF_SIZE];
    wchar_t    *buf_dynamic = NULL;
    wchar_t    *buf         = buf_static;
    size_t      wcssz, u8sz;

    // Set locale to system default when first run
    if( !runned )
    {
        runned = true;
#ifdef WINCE
        // Windows CE did not support "setlocale"
#else
        setlocale(LC_ALL, "");
#endif
    }

    // Calculate buffer size needed
    wcssz = mbstowcs(NULL, src, 0) + 1;
    if( !wcssz ) return 0;
    // Buffer allocate if needed
    if( wcssz > UTFCONV_BUF_SIZE )
    {
        buf = buf_dynamic = malloc( wcssz * sizeof(wchar_t) );
        if( !buf ) return 0;
    }

    // Translate to wide-character format
    mbstowcs(buf, src, wcssz);
    // Calculate destination size needed
    u8sz = utfconv_wcs_to_utf8(NULL, 0, buf);

    // Translate to UTF-8 format
    if( !dst )
    {
        // Nothing to do
    }
    else if( dstsize < u8sz )
    {
        u8sz = 0;
    }
    else
    {
        utfconv_wcs_to_utf8(dst, u8sz, buf);
    }

    // Free the buffer if we allocated it
    if( buf_dynamic )
    {
        free(buf_dynamic);
    }

    return u8sz;
}

bool utfconv_is_utf8_encoding(const void *buf, size_t size)
{
    /**
     * @brief 檢查一段資料或字串是否為 UTF-8 格式。
     *
     * @param buf   傳入欲做檢查的資料或字串。
     * @param size  傳入該筆資料的大小。
     * @return 當傳入資料被判別為 UTF-8 格式時傳回 true；否則傳回 false。
     */
    uint8_t *dat         = (uint8_t*)buf;
    size_t   nread;
    size_t   nread_total = 0;
    size_t   nerror      = 0;

    if( !dat || !size ) return false;

    while( size )
    {
        size_t nbits = count_leadingbits(*dat);

        switch( nbits )
        {
        case 0:
            nread = 1;
            break;

        case 1:
        case 7:
        case 8:
            nread = 1;
            ++nerror;
            break;

        default:
            nread = ( nbits == 1 + count_u8pairs(dat+1) )?
                    ( nbits ):( 1 );
            if( nread == 1 ) ++nerror;

        }

        if( nread > size ) return false;

        nread_total += nread;
        dat         += nread;
        size        -= nread;
    }

    return !nerror;
}
