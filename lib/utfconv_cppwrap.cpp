#include <stdexcept>
#include <vector>
#include "utfconv_cppwrap.h"

using namespace std;
using namespace UtfConv;

u16string UtfConv::Utf32ToUtf16(const u32string &src)
{
    /**
     * @brief Convert UTF-32 string to UTF-16.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf32_to_utf16(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char16_t> buffer(size);
    size = utfconv_utf32_to_utf16(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

string UtfConv::Utf32ToUtf8(const u32string &src)
{
    /**
     * @brief Convert UTF-32 string to UTF-8.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf32_to_utf8(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char> buffer(size);
    size = utfconv_utf32_to_utf8(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

u32string UtfConv::Utf16ToUtf32(const u16string &src)
{
    /**
     * @brief Convert UTF-16 string to UTF-32.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf16_to_utf32(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char32_t> buffer(size);
    size = utfconv_utf16_to_utf32(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

u32string UtfConv::Utf8ToUtf32(const string &src)
{
    /**
     * @brief Convert UTF-8 string to UTF-32.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf8_to_utf32(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char32_t> buffer(size);
    size = utfconv_utf8_to_utf32(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

string UtfConv::Utf16ToUtf8(const u16string &src)
{
    /**
     * @brief Convert UTF-16 string to UTF-8.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf16_to_utf8(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char> buffer(size);
    size = utfconv_utf16_to_utf8(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

u16string UtfConv::Utf8ToUtf16(const string &src)
{
    /**
     * @brief Convert UTF-8 string to UTF-16.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf8_to_utf16(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char16_t> buffer(size);
    size = utfconv_utf8_to_utf16(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

wstring UtfConv::Utf8ToWcs(const string &src)
{
    /**
     * @brief Convert UTF-8 string to wide.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf8_to_wcs(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<wchar_t> buffer(size);
    size = utfconv_utf8_to_wcs(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

string UtfConv::WcsToUtf8(const wstring &src)
{
    /**
     * @brief Convert wide string to UTF-8.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_wcs_to_utf8(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char> buffer(size);
    size = utfconv_wcs_to_utf8(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

string UtfConv::Utf8ToMbs(const string &src)
{
    /**
     * @brief Convert UTF-8 string to MBS.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_utf8_to_mbs(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char> buffer(size);
    size = utfconv_utf8_to_mbs(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}

string UtfConv::MbsToUtf8(const string &src)
{
    /**
     * @brief Convert MBS string to UTF-8.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
    size_t size = utfconv_mbs_to_utf8(NULL, 0, src.c_str());
    if( !size ) throw domain_error(__func__);

    vector<char> buffer(size);
    size = utfconv_mbs_to_utf8(buffer.data(), buffer.size(), src.c_str());
    if( !size ) throw domain_error(__func__);

    return buffer.data();
}
