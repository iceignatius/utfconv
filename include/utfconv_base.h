/**
 * @file
 * @brief UTF converter (include utfconv.h).
 */
#ifndef _UTFCONV_IMPL_H_
#define _UTFCONV_IMPL_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <uchar.h>

#ifdef __cplusplus
extern "C" {
#endif

// Type definition

#ifndef _TCHAR_DEFINED
#   define UTFCONV_TCHAR_DEFINED
#   if defined(_WIN32) && defined(_UNICODE)
#       define TCHAR wchar_t
#   else
#       define TCHAR char
#   endif
#endif

/**
 * @name Character translation
 * @{
 */

size_t utfconv_utf32char_to_utf16char(char16_t dst[2], char32_t src);
size_t utfconv_utf32char_to_utf8char(char dst[6], char32_t src);

size_t utfconv_utf16char_to_utf32char(char32_t *dst, const char16_t *src);
size_t utfconv_utf8char_to_utf32char(char32_t *dst, const char *src);

/**
 * @}
 */

/**
 * @file
 * @note 基於效率的考量，所有字串轉換函式不會先檢查傳入的緩衝區大小是否足夠。
 *       除了無緩衝區傳入的狀況外，函式會直接開始轉換程序，
 *       在轉換過程中發現緩衝空間不足時才會傳回轉換失敗的結果。
 *       亦即無論資料是否轉換成功，緩衝區都會被寫入資料。
 */

/**
 * @name Byte-to-byte data copy
 * @{
 */

size_t utfconv_utf8_to_utf8(char *dst, size_t dstsize, const char *src);

/**
 * @}
 */

/**
 * @name String translation ( UTF <--> UTF )
 * @{
 */

size_t utfconv_utf32_to_utf16(char16_t *dst, size_t dstsize, const char32_t *src);
size_t utfconv_utf32_to_utf8(char *dst, size_t dstsize, const char32_t *src);

size_t utfconv_utf16_to_utf32(char32_t *dst, size_t dstsize, const char16_t *src);
size_t utfconv_utf8_to_utf32(char32_t *dst, size_t dstsize, const char *src);

size_t utfconv_utf16_to_utf8(char *dst, size_t dstsize, const char16_t *src);
size_t utfconv_utf8_to_utf16(char16_t *dst, size_t dstsize, const char *src);

/**
 * @}
 */

/**
 * @name String translation ( UTF-8 <--> WCS )
 * @{
 */

size_t utfconv_utf8_to_wcs(wchar_t *dst, size_t dstsize, const char *src);
size_t utfconv_wcs_to_utf8(char *dst, size_t dstsize, const wchar_t *src);

/**
 * @}
 */

/**
 * @name String translation ( UTF-8 <--> Traditional MBS )
 * @{
 */

size_t utfconv_utf8_to_mbs(char *dst, size_t dstsize, const char *src);
size_t utfconv_mbs_to_utf8(char *dst, size_t dstsize, const char *src);

/**
 * @}
 */

/**
 * @name String translation ( UTF-8 <--> Windows CHAR string )
 * @{
 */

static inline
size_t utfconv_utf8_to_winstr(TCHAR *dst, size_t dstsize, const char *src)
{
    /**
     * @brief Translate UTF-8 string to Windows TCHAR format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 TCHAR 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 TCHAR 為單位。
     */
#if defined(_WIN32) && defined(_UNICODE)
    return utfconv_utf8_to_wcs(dst, dstsize, src);
#elif defined(_WIN32) && !defined(_UNICODE)
    return utfconv_utf8_to_mbs(dst, dstsize, src);
#else
    return utfconv_utf8_to_utf8(dst, dstsize, src);
#endif
}

static inline
size_t utfconv_winstr_to_utf8(char *dst, size_t dstsize, const TCHAR *src)
{
    /**
     * @brief Translate Windows TCHAR string to UTF-8 format.
     *
     * @param dst       用來接收轉換結果的緩衝區，可為 NULL。
     * @param dstsize   緩衝區大小，以 byte 為單位。
     * @param src       來源字串。
     * @return  轉換成功傳回寫入緩衝區的陣列元素數量(含 null-terminator)；
     *          轉換失敗傳回零；
     *          若 dst 為 NULL 則傳回 dst 緩衝區所需的最小大小(含 null-terminator)，
     *          以 byte 為單位。
     */
#if defined(_WIN32) && defined(_UNICODE)
    return utfconv_wcs_to_utf8(dst, dstsize, src);
#elif defined(_WIN32) && !defined(_UNICODE)
    return utfconv_mbs_to_utf8(dst, dstsize, src);
#else
    return utfconv_utf8_to_utf8(dst, dstsize, src);
#endif
}

/**
 * @}
 */

/**
 * @name String format identify
 * @{
 */

bool utfconv_is_utf8_encoding(const void *buf, size_t size);

/**
 * @}
 */

#ifdef __cplusplus
}   // extern "C"
#endif

#ifdef UTFCONV_TCHAR_DEFINED
    #undef TCHAR
    #undef UTFCONV_TCHAR_DEFINED
#endif

#endif
