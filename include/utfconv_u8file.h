/**
 * @file
 * @brief STDIO functions with UTF-8 file name support (include utfconv.h).
 */
#ifndef _UTFCONV_U8FILE_H_
#define _UTFCONV_U8FILE_H_

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef __cplusplus
extern "C" {
#endif

FILE* utfconv_fopen(const char *filename, const char *mode);
FILE* utfconv_freopen(const char *filename, const char *mode, FILE *stream);
int utfconv_remove(const char *filename);
int utfconv_rename(const char *oldname, const char *newname);

int utfconv_stat(const char *pathname, struct stat *statbuf);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
