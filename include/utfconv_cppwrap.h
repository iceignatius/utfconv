/**
 * @file
 * @brief UTF converter C++ wrapper (include utfconv.h).
 */
#ifndef _UTFCONV_CPPWRAPPER_H_
#define _UTFCONV_CPPWRAPPER_H_

#include <string>
#include "utfconv_base.h"

/*
 * To avoid the unknown type problem when using Doxygen.
 */
#ifdef DOXYGEN
namespace std
{
typedef basic_string<char>      string;
typedef basic_string<wchar_t>   wstring;
typedef basic_string<char16_t>  u16string;
typedef basic_string<char32_t>  u32string;
}
#endif

/**
 * @brief UTF converter C++ wrapper.
 */
namespace UtfConv
{

/**
 * @name Types
 * @{
 */

#ifdef _UNICODE
typedef std::wstring WinString; ///< Windows string.
#else
typedef std::string WinString;  ///< Windows string.
#endif

/**
 * @}
 */

/**
 * @name String translation ( UTF <--> UTF )
 * @{
 */

std::u16string  Utf32ToUtf16(const std::u32string &src);
std::string     Utf32ToUtf8(const std::u32string &src);

std::u32string  Utf16ToUtf32(const std::u16string &src);
std::u32string  Utf8ToUtf32(const std::string &src);

std::string     Utf16ToUtf8(const std::u16string &src);
std::u16string  Utf8ToUtf16(const std::string &src);

/**
 * @}
 */

/**
 * @name String translation ( UTF-8 <--> WCS )
 * @{
 */

std::wstring    Utf8ToWcs(const std::string &src);
std::string     WcsToUtf8(const std::wstring &src);

/**
 * @}
 */

/**
 * @name String translation ( UTF-8 <--> Traditional MBS )
 * @{
 */

std::string Utf8ToMbs(const std::string &src);
std::string MbsToUtf8(const std::string &src);

/**
 * @}
 */

/**
 * @name String translation ( UTF-8 <--> Windows CHAR string )
 * @{
 */

inline
WinString Utf8ToWinstr(const std::string &src)
{
    /**
     * @brief Convert UTF-8 string to Windows string.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
#if defined(_WIN32) && defined(_UNICODE)
    return Utf8ToWcs(src);
#elif defined(_WIN32) && !defined(_UNICODE)
    return Utf8ToMbs(src);
#else
    return src;
#endif
}

inline
std::string WinstrToUtf8(const WinString &src)
{
    /**
     * @brief Convert Windows string to UTF-8.
     * @param src The source string.
     * @return The result string.
     * @remarks Exception std::domain_error may be thorwn if convert failed.
     */
#if defined(_WIN32) && defined(_UNICODE)
    return WcsToUtf8(src);
#elif defined(_WIN32) && !defined(_UNICODE)
    return MbsToUtf8(src);
#else
    return src;
#endif
}

/**
 * @}
 */

}   // namespace UtfConv

#endif
