/**
 * @file
 * @brief UTF converter.
 */
#ifndef _UTFCONV_H_
#define _UTFCONV_H_

#include "utfconv_config.h"
#include "utfconv_base.h"
#ifdef UTFCONV_U8FILE_ENABLED
#   include "utfconv_u8file.h"
#endif
#if defined(__cplusplus) && defined(UTFCONV_CPPWRAPPER_ENABLED)
#   include "utfconv_cppwrap.h"
#endif

#endif
