#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>
#include "utfconv.h"
#include "test_samples.h"
#include "format_detect_test.h"

static
void detect_text_test(void **state)
{

    assert_true(utfconv_is_utf8_encoding(format_text_utf8, sizeof(format_text_utf8)));
    assert_false(utfconv_is_utf8_encoding(format_text_big5, sizeof(format_text_big5)));
}

int format_detect_test(void)
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(detect_text_test),
    };

    return
        cmocka_run_group_tests_name(
            "format detect test",
            tests,
            NULL,
            NULL);
}
