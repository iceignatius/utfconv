#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>
#include <cmocka.h>
#include "utfconv.h"
#include "test_samples.h"
#include "char_conv_test.h"

static
void char_conv_test_32_16(void **state)
{
    char16_t buf[2];
    size_t cnt, cnt_target;

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample01_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample01_16, cnt_target*sizeof(char16_t));

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample02_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample02_16, cnt_target*sizeof(char16_t));

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample03_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample03_16, cnt_target*sizeof(char16_t));

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample04_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample04_16, cnt_target*sizeof(char16_t));

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample05_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample05_16, cnt_target*sizeof(char16_t));

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample06_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample06_16, cnt_target*sizeof(char16_t));

    cnt_target = 2;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample07_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample07_16, cnt_target*sizeof(char16_t));

    cnt_target = 2;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf16char(buf, sample08_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample08_16, cnt_target*sizeof(char16_t));
}

static
void char_conv_test_32_8(void **state)
{
    char buf[6];
    int cnt, cnt_target;

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample01_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample01_8, cnt_target);

    cnt_target = 1;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample02_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample02_8, cnt_target);

    cnt_target = 2;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample03_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample03_8, cnt_target);

    cnt_target = 2;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample04_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample04_8, cnt_target);

    cnt_target = 3;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample05_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample05_8, cnt_target);

    cnt_target = 3;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample06_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample06_8, cnt_target);

    cnt_target = 4;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample07_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample07_8, cnt_target);

    cnt_target = 4;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample08_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample08_8, cnt_target);

    cnt_target = 5;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample09_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample09_8, cnt_target);

    cnt_target = 5;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample10_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample10_8, cnt_target);

    cnt_target = 6;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample11_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample11_8, cnt_target);

    cnt_target = 6;
    memset(buf, 0, sizeof(buf));
    cnt = utfconv_utf32char_to_utf8char(buf, sample12_32);
    assert_int_equal(cnt, cnt_target);
    assert_memory_equal(buf, sample12_8, cnt_target);
}

static
void char_conv_test_16_32(void **state)
{
    int cnt, cnt_target;
    char32_t code;
    char16_t invalid_utf16[] = { 0xDC00 , 0xD800 };

    assert_int_equal(1, utfconv_utf16char_to_utf32char(&code, invalid_utf16));
    assert_int_equal(code, (uint16_t)invalid_utf16[0]);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample01_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample01_32);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample02_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample02_32);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample03_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample03_32);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample04_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample04_32);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample05_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample05_32);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample06_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample06_32);

    cnt_target = 2;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample07_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample07_32);

    cnt_target = 2;
    code = 0;
    cnt = utfconv_utf16char_to_utf32char(&code, sample08_16);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample08_32);
}

static
void char_conv_test_8_32(void **state)
{
    int cnt, cnt_target;
    char32_t code;

    assert_int_equal(1, utfconv_utf8char_to_utf32char(&code, "\xFF\xFF"));
    assert_int_equal(code, 0xFF);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample01_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample01_32);

    cnt_target = 1;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample02_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample02_32);

    cnt_target = 2;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample03_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample03_32);

    cnt_target = 2;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample04_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code,sample04_32);

    cnt_target = 3;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample05_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample05_32);

    cnt_target = 3;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample06_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample06_32);

    cnt_target = 4;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample07_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample07_32);

    cnt_target = 4;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample08_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample08_32);

    cnt_target = 5;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample09_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample09_32);

    cnt_target = 5;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample10_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample10_32);

    cnt_target = 6;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample11_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample11_32);

    cnt_target = 6;
    code = 0;
    cnt = utfconv_utf8char_to_utf32char(&code, sample12_8);
    assert_int_equal(cnt, cnt_target);
    assert_int_equal(code, sample12_32);
}

int char_conv_test(void)
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(char_conv_test_32_16),
        cmocka_unit_test(char_conv_test_32_8),
        cmocka_unit_test(char_conv_test_16_32),
        cmocka_unit_test(char_conv_test_8_32),
    };

    return
        cmocka_run_group_tests_name(
            "character conversion test",
            tests,
            NULL,
            NULL);
}
