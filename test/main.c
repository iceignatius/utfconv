#include "char_conv_test.h"
#include "str_conv_test.h"
#include "format_detect_test.h"

int main(int argc, char *argv[])
{
    int ret;

    if(( ret = char_conv_test() )) return ret;
    if(( ret = str_conv_test() )) return ret;
    if(( ret = format_detect_test() )) return ret;

    return 0;
}
