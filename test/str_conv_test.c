#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>
#include "utfconv.h"
#include "test_samples.h"
#include "str_conv_test.h"

#define BUFSIZE 1024

static
void str_conv_test_8_8(void **state)
{
    const char data0[] = "Byte-to-byte data copy test";
    char data1[BUFSIZE];
    size_t data1sz;

    data1sz = utfconv_utf8_to_utf8(NULL, 0, data0);
    assert_int_equal(data1sz, sizeof(data0));

    data1sz = utfconv_utf8_to_utf8(data1, sizeof(data1), data0);
    assert_int_equal(data1sz, sizeof(data0));
    assert_memory_equal(data1, data0, sizeof(data0));
}

static
void str_conv_test_32_16(void **state)
{
    char16_t buf[BUFSIZE];
    size_t nwrite;

    assert_int_equal(strcnt_16, utfconv_utf32_to_utf16(NULL, 0, string_32));
    nwrite = utfconv_utf32_to_utf16(buf, BUFSIZE, string_32);
    assert_int_equal(nwrite, strcnt_16);
    assert_memory_equal(buf, string_16, nwrite*sizeof(char16_t));
}

static
void str_conv_test_32_8(void **state)
{
    char buf[BUFSIZE];
    size_t nwrite;

    assert_int_equal(strcnt_8, utfconv_utf32_to_utf8(NULL, 0, string_32));
    nwrite = utfconv_utf32_to_utf8(buf, BUFSIZE, string_32);
    assert_int_equal(nwrite, strcnt_8);
    assert_memory_equal(buf, string_8, nwrite*sizeof(char));
}

static
void str_conv_test_16_32(void **state)
{
    char32_t buf[BUFSIZE];
    size_t nwrite;

    assert_int_equal(strcnt_32, utfconv_utf16_to_utf32(NULL, 0, string_16));
    nwrite = utfconv_utf16_to_utf32(buf, BUFSIZE, string_16);
    assert_int_equal(nwrite, strcnt_32);
    assert_memory_equal(buf, string_32, nwrite*sizeof(char32_t));
}

static
void str_conv_test_8_32(void **state)
{
    char32_t buf[BUFSIZE];
    size_t nwrite;

    assert_int_equal(strcnt_32, utfconv_utf8_to_utf32(NULL, 0, string_8));
    nwrite = utfconv_utf8_to_utf32(buf, BUFSIZE, string_8);
    assert_int_equal(nwrite, strcnt_32);
    assert_memory_equal(buf, string_32, nwrite*sizeof(char32_t));
}

static
void str_conv_test_16_8(void **state)
{
    char buf[BUFSIZE];
    size_t nwrite;

    assert_int_equal(strcnt_8, utfconv_utf16_to_utf8(NULL, 0, string_16));
    nwrite = utfconv_utf16_to_utf8(buf, BUFSIZE, string_16);
    assert_int_equal(nwrite, strcnt_8);
    assert_memory_equal(buf, string_8, nwrite*sizeof(char));
}

static
void str_conv_test_8_16(void **state)
{
    char16_t buf[BUFSIZE];
    size_t nwrite;

    assert_int_equal(strcnt_16, utfconv_utf8_to_utf16(NULL, 0, string_8));
    nwrite = utfconv_utf8_to_utf16(buf, BUFSIZE, string_8);
    assert_int_equal(nwrite, strcnt_16);
    assert_memory_equal(buf, string_16, nwrite*sizeof(char16_t));
}

static
void str_conv_test_8_wide(void **state)
{
    wchar_t wcsbuf[BUFSIZE];
    size_t wcssz =
        utfconv_utf8_to_wcs(
            wcsbuf,
            sizeof(wcsbuf)/sizeof(wcsbuf[0]),
            string_text_ascii);
    assert_int_equal(wcssz, sizeof(string_text_ascii));
    assert_memory_equal(wcsbuf, string_text_wide, sizeof(string_text_wide));


    char u8buf[BUFSIZE];
    size_t u8sz;
    u8sz = utfconv_wcs_to_utf8(u8buf, sizeof(u8buf), string_text_wide);
    assert_int_equal(u8sz, sizeof(string_text_ascii));
    assert_memory_equal(u8buf, string_text_ascii, sizeof(string_text_ascii));
}

static
void str_conv_test_8_mbs(void **state)
{
    char mbsbuf[BUFSIZE];
    size_t mbssz = utfconv_utf8_to_mbs(mbsbuf, sizeof(mbsbuf), string_text_ascii);
    assert_int_equal(mbssz, sizeof(string_text_ascii));
    assert_memory_equal(mbsbuf, string_text_ascii, sizeof(string_text_ascii));

    char u8buf[BUFSIZE];
    size_t u8sz = utfconv_mbs_to_utf8(u8buf, sizeof(u8buf), string_text_ascii);
    assert_int_equal(u8sz, sizeof(string_text_ascii));
    assert_memory_equal(u8buf, string_text_ascii, sizeof(string_text_ascii));
}

int str_conv_test(void)
{
    struct CMUnitTest tests[] =
    {
        cmocka_unit_test(str_conv_test_8_8),
        cmocka_unit_test(str_conv_test_32_16),
        cmocka_unit_test(str_conv_test_32_8),
        cmocka_unit_test(str_conv_test_16_32),
        cmocka_unit_test(str_conv_test_8_32),
        cmocka_unit_test(str_conv_test_16_8),
        cmocka_unit_test(str_conv_test_8_16),
        cmocka_unit_test(str_conv_test_8_wide),
        cmocka_unit_test(str_conv_test_8_mbs),
    };

    return
        cmocka_run_group_tests_name(
            "string conversion test",
            tests,
            NULL,
            NULL);
}
